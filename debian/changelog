riemann-c-client (1.10.4-4) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.2.1 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Replaced obsolete pkg-config build dependency with pkgconf.
  * Moved to debhelper compat level 12.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 29 Apr 2024 08:22:28 +0200

riemann-c-client (1.10.4-3) unstable; urgency=medium

  * QA upload.
  * Drop Vcs-* fields and gbp.conf.

  [ Romain Tartière ]
  * Fix GnuTLS send/recv.

 -- Bastian Germann <bage@debian.org>  Mon, 14 Aug 2023 00:08:48 +0200

riemann-c-client (1.10.4-2) unstable; urgency=medium

  * Orphaning the package.

 -- Gergely Nagy <algernon@madhouse-project.org>  Thu, 03 Jan 2019 18:09:25 +0100

riemann-c-client (1.10.4-1) unstable; urgency=medium

  * New upstream release.
    + Fixes warnings when compiling with recent check. (Closes: #916232)
  * Standards-Version bumped to 4.2.1, no changes necessary.
  * Added a "Build-Depends-Package" field to libriemann-client0.symbols.
    Thanks Lintian!

 -- Gergely Nagy <algernon@madhouse-project.org>  Thu, 13 Dec 2018 01:33:20 +0100

riemann-c-client (1.10.3-3) unstable; urgency=medium

  * Add libgnutls28-dev to the autopkgtest dependencies.

 -- Gergely Nagy <algernon@madhouse-project.org>  Tue, 21 Aug 2018 23:52:21 +0200

riemann-c-client (1.10.3-2) unstable; urgency=medium

  * Added an autopkgtest testsuite.

 -- Gergely Nagy <algernon@madhouse-project.org>  Mon, 20 Aug 2018 08:13:42 +0200

riemann-c-client (1.10.3-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version bumped to 4.2.0:
    - debian/copyright updated to use a https:// URL for the Format: stanza.
    - All extra priorities were changed to optional.
    - NEWS is installed as such, instead of as a changelog.
  * Updated debian/copyright: I have assigned my Debian copyrights to the
    SFC.
  * Updated the Homepage and VCS-* URLs (along with the watch file) to
    point to our new home.
  * Updated the symbol file with new symbols.
  * Build with all hardening flags enabled.
  * Override the hardening-no-fortify-functions lintian tag.
  * Mark libriemann-client-dev Multi-Arch: same.

 -- Gergely Nagy <algernon@madhouse-project.org>  Sun, 19 Aug 2018 20:41:53 +0200

riemann-c-client (1.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version bumped to 3.9.8, no changes necessary.
  * Bump debhelper compat level to 10, and remove obsolete dependencies
    and code.

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 30 Sep 2016 18:44:28 +0200

riemann-c-client (1.8.1-2) unstable; urgency=medium

  * Disable parallel build for now, to make sure the package builds
    everywhere.

 -- Gergely Nagy <algernon@madhouse-project.org>  Tue, 29 Sep 2015 12:05:16 +0200

riemann-c-client (1.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Updated the symbol file with new symbols.
  * Include the API docs in the -dev package.

 -- Gergely Nagy <algernon@madhouse-project.org>  Mon, 28 Sep 2015 13:46:39 +0200

riemann-c-client (1.7.0-1) unstable; urgency=low

  * New upstream release.
  * Forcibly enable TLS support, to make sure it is not off by accident.
  * Updated the symbol file with new symbols.

 -- Gergely Nagy <algernon@madhouse-project.org>  Mon, 04 May 2015 11:36:08 +0200

riemann-c-client (1.6.1-1) experimental; urgency=low

  * New upstream release.
  * Updated the symbol file with new symbols.
  * NEWS got renamed to NEWS.md, use that as the upstream changelog.

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 24 Apr 2015 10:30:58 +0200

riemann-c-client (1.5.0-1) experimental; urgency=low

  * New upstream release.
  * Add libgnutls28-dev to Build-Depends, to include TLS support too.
  * Updated the symbol file with new symbols.
  * Bump Standards-Version to 3.9.6 (no changes necessary)

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 27 Mar 2015 16:32:42 +0100

riemann-c-client (1.4.0-1) experimental; urgency=low

  * New upstream release.
  * Drop the ruby-ronn build dependency, the manual page was rewritten in
    nroff, upstream.
  * Updated the symbol file with new symbols.
  * Copyright years updated in debian/copyright.
  * Drop the documentation from libriemann-client-dev. A -doc package can
    be introduced later, if need be.

 -- Gergely Nagy <algernon@madhouse-project.org>  Tue, 17 Mar 2015 12:11:31 +0100

riemann-c-client (1.2.0-2) unstable; urgency=medium

  * Instead of libriemann-client-dev depending on libprotobuf-c0-dev,
    depend on the appropriate version, determined at build time.

 -- Gergely Nagy <algernon@madhouse-project.org>  Sat, 26 Jul 2014 23:35:10 +0200

riemann-c-client (1.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depend on libprotobuf-c-dev libprotobuf-c0-dev, for the
    protobuf-c transition and wheezy backportability.
  * Update the copyright years in debian/copyright.

 -- Gergely Nagy <algernon@madhouse-project.org>  Fri, 25 Jul 2014 16:02:27 +0200

riemann-c-client (1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Added libjson-c-dev to Build-Depends, so that the riemann-c-client
    package builds with JSON support.

 -- Gergely Nagy <algernon@madhouse-project.org>  Thu, 23 Jan 2014 17:24:13 +0100

riemann-c-client (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Symbol file updated with new symbols.

 -- Gergely Nagy <algernon@madhouse-project.org>  Sun, 22 Dec 2013 11:50:26 +0100

riemann-c-client (1.0.2-1) unstable; urgency=low

  * Initial release. (Closes: #728591)

 -- Gergely Nagy <algernon@madhouse-project.org>  Sat, 23 Nov 2013 12:20:53 +0100
